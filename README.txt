DUT Informatique - Projet Web S4
Eugénie DEUDON

Sujet : variante du site de cocktails
Adresse du site : http://edeudon-projetweb.alwaysdata.net/
Librairies : JQuery

Compte de test:
    Pseudo : Billy
    Mot de passe : fromage
Il n'y a qu'un seul niveau d'autorisation.

Fonctionnalités :
Celles implémentées sont les suivantes :
--> Au chargement du site :
    - Affichage de tous les cocktails de la base sur la page d'accueil
    - On peut cliquer sur les noms de cocktails pour afficher plus d'informations
    - Menu cliquables en haut à gauche et à droite (menu global et profil)
    - Si vous êtes connecté, votre pseudo devrait apparaître à côté du bouton pour le profil
    - Bouton pour le retour à l'accueil (utile une fois dans les menus)
--> Dans le menu global :
    - Sous-menu de recherche par ingrédient
        --> Ce sous-menu vous proposera une liste d'ingrédients. 
        Sélectionnez-en quelques-uns, il vous renverra à l'accueil en mettant à jour la liste avec les cocktails possibles pour les ingrédients sélectionnés.
--> Dans le menu profil :
    - Si déconnecté :
        - Connexion
        - Création de compte (nécessite de se connecter après)
    - Si connecté :
        - Déconnexion
    (Attention, un rafraîchissement de la page déconnecte l'utilisateur)
Comme vous pourrez le voir sur le site et dans le code, j'avais prévu plusieurs fonctions que je n'ai pas eu le temps d'implémenter...

Base de données : 
    https://phpmyadmin.alwaysdata.com/
    Utilisateur MySQL : 
    Nom : 230789 
    Mot de passe : granola12
    
Ressources pour les cocktails : https://www.1001cocktails.com
https://cocktailmolotov.org/guides/le-top-20-des-ingredients-a-avoir-chez-soi-pour-preparer-des-cocktails-premiere-partie/
