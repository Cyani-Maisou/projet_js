<?php
    require 'database_access.php';

    $database = new DB_Access();
    if(isset($_POST['nom']) && isset($_POST['difficulte']) && isset($_POST['ingredients'])){
        $nom = $_POST['nom'];
        $difficulte = $_POST['difficulte'];
        $ingredients = $_POST['ingredients'];
        json_encode($ingredients);
        $database->addCocktail($nom, $difficulte, $ingredients);
    }
?>
