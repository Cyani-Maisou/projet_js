<?php
    session_start();
    $obj = new stdClass();
    $obj->is_connected = false;
    if(isset($_SESSION['pseudo'])){
        $obj->is_connected = true;
    } else {
        $obj->is_connected = false;
    }
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    echo json_encode($obj);