<?php
    require 'database_access.php';

    $database = new DB_Access();
    $obj = new stdClass();
    $obj->parcours = "entrée dans le fichier";
    $obj->infos_cocktail = null;
    $obj->nom = null;

    if(!empty($_POST)){
        $obj->parcours .= ", $_POST non vide";
        if(isset($_POST['nom'])) {
            $obj->parcours .= ", isset ok";
            $nom = $_POST['nom'];
            $obj->nom = $nom;
            $obj->infos_cocktail = $database->infoCocktail($nom);
        } else {
            $obj->parcours .= ", isset non ok";
        }
    } else {
        $obj->parcours .= " $_POST vide";
    }

    echo json_encode($obj);


