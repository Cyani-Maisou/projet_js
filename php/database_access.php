<?php
  //use mysqli;

  class DB_Access{
    private $mysqli;

    public function __construct() {
      // Connexion mysqli vers la base de données
      $this->mysqli = new mysqli('mysql-edeudon-projetweb.alwaysdata.net', '230789', 'granola12', 'edeudon-projetweb_cocktails');

      if ($this->mysqli->connect_errno) {
        error_log("pb de connexion avec mysqli (fichier:database_access.php)",3,"logs");
      }
    }

    public function userExists($user){
      $prepared_query = $this->mysqli->prepare('SELECT pseudo FROM UTILISATEUR WHERE pseudo=?');
      $prepared_query->bind_param('s', $user);
      $prepared_query->execute();
      $result = $prepared_query->get_result();
      if (!$result) {
        return false;
      } else {
        $user_data = $result->fetch_assoc();
        if($user_data['pseudo'] == $user){
          return true;
        }else{
          return false;
        }
      }
    }

    public function login($user, $password){
      $prepared_query = $this->mysqli->prepare('SELECT pseudo, motdepasse FROM UTILISATEUR WHERE pseudo=?');
      $prepared_query->bind_param('s', $user);
      $prepared_query->execute();
      $result = $prepared_query->get_result();
      if (!$result) {
        return false;
      } else {
        $user_data = $result->fetch_assoc();
        if($user_data['pseudo'] == $user && $user_data['motdepasse'] == $password){
          return true;
        }else{
          return false;
        }
      }
    }

    public function register($user, $password){
      $prepared_query = $this->mysqli->prepare('INSERT INTO UTILISATEUR (pseudo, motdepasse) VALUES (?, ?)');
      $prepared_query->bind_param('ss', $user, $password);
        if($prepared_query->execute()){
          return true;
        } else {
          return false;
        }
      }

    public function getCocktailsFromIngredient($ingredient){
      $cocktails_list = array();
      $prepared_query = $this->mysqli->prepare('SELECT nom FROM COCKTAILS WHERE ingredients LIKE ?');
      $str = "%".$ingredient."%";
      $prepared_query->bind_param('s', $str);
      $prepared_query->execute();
      $result = $prepared_query->get_result();
      if (!$result) {
        //todo : gérer erreur
      } else {
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
          $cocktails_list[] = $row;
        }
        return $cocktails_list; //envoie un objet json représentant la liste
      }
      return null; //retournera null en cas de problème
    }

    public function getCocktailsList(){
      $testarray = array();
      $prepared_query = $this->mysqli->prepare('SELECT nom FROM COCKTAILS');
      $prepared_query->execute(); // exécute la requête
      $result = $prepared_query->get_result(); // stocke le résultat de la requête
      if (!$result) {
                //todo : gérer erreur
            } else {
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
          $testarray[] = $row;
        }
        return $testarray; //envoie un objet json représentant la liste
      }
      return null; //retournera null en cas de problème
    }

    public function addCocktail($nom, $difficulte, $ingredients){
      //INSERT INTO `COCKTAILS` (`nom`, `difficulte`, `ingredients`) VALUES ('a', 'facile', 'b')
      $prepared_query = $this->mysqli->prepare('INSERT INTO COCKTAILS (nom, difficulte, ingredients) VALUES (?, ?, ?)');
      $prepared_query->bind_param('sss', $nom, $difficulte, $ingredients);
      if(!$prepared_query->execute()){
        error_log("pb ajout de cocktail (exécution de la requête, fichier database_access.php", 3, "logs");
      }
      return true;
    }
    public function infoCocktail($nom){
      $prepared_query = $this->mysqli->prepare('SELECT nom, difficulte, ingredients FROM COCKTAILS WHERE nom = ?');
      $prepared_query->bind_param('s', $nom);
      $prepared_query->execute();
      $result = $prepared_query->get_result();
      $infos = array();
      if (!$result) {
        //todo : gérer erreur
      } else {
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
          $infos[] = $row;
        }
        return $infos; //envoie un objet json représentant la liste
      }
    }

    public function __destruct(){
      $this->mysqli->close();
    }
  }


?>
