<?php
    require 'database_access.php';

    $database = new DB_Access();
    $obj = new stdClass();
    $obj->debug = "entrée du fichier \n";
    $obj->issetpost = false;
    $obj->userexists = false;
    $obj->useradded = false;

    if(isset($_POST['pseudo']) && isset($_POST['mdp'])){
        $obj->debug .= "condition isset $_POST ok \n";
        $obj->issetpost = true;
        $pseudo = $_POST['pseudo'];
        $mdp = $_POST['mdp'];
        if(!$database->userExists($pseudo)){ // on vérifie que l'utilisateur n'existe pas
            $obj->debug .= "entrée dans la condition login (vérifier que l'utilisateur n'existe pas";
            $obj->userexists = false;
            if($database->register($pseudo, $mdp)){
                $obj->useradded = true;
            } else {
                $obj->useradded = false;
            }
        } else {
            $obj->userexists = true;
        }
    } else {
        $obj->issetpost = false;
    }

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    echo json_encode($obj);