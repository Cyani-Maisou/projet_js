<?php
    require 'database_access.php';

    $database = new DB_Access();
    $obj = new stdClass();
    session_start();
    $obj->debug = "entrée du fichier \n";
    $obj->issetpost = false;
    $obj->connection_allowed = false;

    if(isset($_POST['pseudo']) && isset($_POST['mdp'])){
        $obj->debug .= "condition isset $_POST ok \n";
        $obj->issetpost = true;
        $pseudo = $_POST['pseudo'];
        $mdp = $_POST['mdp'];
        if($database->login($pseudo, $mdp)){ // on vérifie que l'utilisateur existe et que le mot de passe corresponde
            $obj->debug .= "entrée dans la condition login (vérifier que l'utilisateur existe\n";
            $obj->connection_allowed = true;
            $_SESSION['pseudo'] = $pseudo;
            $obj->pseudo = $pseudo;
            $obj->mdp = $mdp;
        } else {
            $obj->connection_allowed = false;
        }
    } else {
        $obj->issetpost = false;
    }

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    echo json_encode($obj);

