<?php
    require 'database_access.php';

    $database = new DB_Access();
    $cocktails_list = $database->getCocktailsList();
    echo json_encode($cocktails_list);
?>
