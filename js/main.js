(function(){
  "use strict";

  const SITE = "http://edeudon-projetweb.alwaysdata.net"
  const TAILLE_ICONES = "48px"; // 18, 24, 36 ou 48 de préférence
  const HEADER_Z_INDEX = 1;
  const MENU_AND_PROFILE_Z_INDEX = 1;
  const LISTE_Z_INDEX = 0;
  const ACCUEIL_Z_INDEX = 2;

  const CSS_BODY = {
    "background-color" : "#ffe0b2"
  }

  const CSS_HEADER = {
    "z-index" : HEADER_Z_INDEX,
    "background-color" : "#fb8c00",
    "display" : "flex",
    "justify-content" : "space-between",
    "align-items" : "center",
    "padding" : "1%",
    "font-size" : "20px"
  }

  const CSS_LISTE = {
    "z-index" : LISTE_Z_INDEX,
    "background-color" : "#ffbd45",
    "text-align" : "center",
    "margin" : "5%",
    "padding" : "1%"
  }

  const CSS_MENU = {
    "z-index" : MENU_AND_PROFILE_Z_INDEX,
    "position" : "absolute",
    "background-color" : "#fad188",
    "height" : 0,
    "width" : "30vw", //30% view width
    "padding-left" : "1%",
    "margin-bottom" : 0,
  }

  const CSS_MENU_CONTENT = {
    "margin-top" : "30%",
    "margin-left" : "5%",
    "font-size" : 24,
    "list-style" : "none"
  }

  const CSS_PROFILE = {
    "z-index" : MENU_AND_PROFILE_Z_INDEX,
    "position" : "absolute",
    "height" : 0,
    "width" : "100%",
    "background-color" : "#fcbf50",
    "padding" : "10%",
    "line-height" : "80px"
  }

  const CSS_ICONS = {
    "font-size" : TAILLE_ICONES,
    "user-select" : "none",
    "cursor" : "pointer"
  }

  const CSS_RECHERCHE_PAR_INGREDIENTS = {
    "padding" : "10%"
  }

  const CSS_BOUTON_ACCUEIL = {
    "text-decoration" : "none",
    "color" : "black",
    "z-index" : ACCUEIL_Z_INDEX,
    "user-select" : "none",
    "cursor" : "pointer"
  }

  let menu_open = false;
  let profile_open = false;

  function lister_cocktails() {
    // récupération de l'id, et nettoyage
    let $liste = $("#liste");
    $liste.empty();
    // ajout du css et contenu statique
    $liste.css(CSS_LISTE);
    $.ajax({
      url: SITE + "/php/cocktails_list.php",
      method: "POST",
      dataType: "json",
    })
        .done(function(data){
          for (let i in data) {
            if(data.hasOwnProperty(i)){ //permet d'éviter un warning
              let cocktail = data[i]["nom"];
              let $div_cocktail = $("<div id='"+cocktail+"'>"+cocktail+"</div></br>");
              $liste.append($div_cocktail);
              $div_cocktail.click(function (cocktail) {
                afficher_cocktail(cocktail.currentTarget.innerText);
              });
            } else {
              alert("Problème lors du chargement de la liste des cocktails.");
              console.log("ce cocktail n'a pas de nom... ou erreur de parcours (fonction lister_cocktails)");
            }
          }
        });
    $liste.css(CSS_LISTE);
  }

  function afficher_cocktail(cocktail){
    let $liste = $("#liste");
    $.when(info_cocktail(cocktail)).then(function(data){
      let info = data['infos_cocktail'][0];
      $liste.empty();
      $liste.append(cocktail + "</br>");
      $liste.append("Difficulté : " + info['difficulte'] + "</br>");
      $liste.append("</br> Ingrédients : </br>");
      let ingredients = JSON.parse(info['ingredients']);
      for (const ingredientsKey in ingredients) {
        $liste.append(ingredientsKey + " : " + ingredients[ingredientsKey] + "</br>");
      }
      $liste.append("<button id='retour'>Retour</button>");
      $("#retour").click(accueil);
    })
  }

  function info_cocktail(cocktail){
    return $.ajax({
      url: SITE + "/php/info_cocktail.php",
      type: "POST",
      data: {"nom" : cocktail},
      dataType: "json"
    }).done(function(data){
      return data;
    }).fail(function(data){
      alert("La récupération des informations sur le cocktail a échoué.");
      console.log("erreur appel ajax pour la récupération d'infos sur le cocktail");
      console.log(data);
    });
  }
  
  function close_menu() {
    let $menu = $("#menu");
    $menu.animate({"height" : "0"}, "fast", "swing", function(){$menu.remove();});
    $("#menu_icon").css("z-index", HEADER_Z_INDEX);
    menu_open = false;
  }
  
  function open_menu() {
    $("body").prepend("<div id=\"menu\"></div>");
    let $menu = $("#menu");
    $menu.css(CSS_MENU);
    $menu.animate({"height" : "100%"}, "slow");
    $("#menu_icon").css("z-index", MENU_AND_PROFILE_Z_INDEX+1);
    $menu.append("<ul>" +
        "<li id='menu_vos_cocktails' class='menu_content'>Vos cocktails </br> [indisponible pour le moment]</li>" +
        "<li id='menu_rechercher' class='menu_content'>Recherche par ingrédients</li>");
    $("#menu_vos_cocktails").click(vos_cocktails_on_click).css("color", "grey");
    $("#menu_rechercher").css("user-select", "none").css("cursor", "pointer").click(rechercher_on_click);
    $(".menu_content").css(CSS_MENU_CONTENT);
    menu_open = true;
  }
  
  function close_profile() {
    let $profile = $("#profile");
    $profile.animate({"height" : "0"}, "fast", "linear", function(){$profile.remove();});
    $("#icone_profil").css("z-index", HEADER_Z_INDEX);
    profile_open = false;
  }
  
  function open_profile() {
    $("body").prepend("<div id=\"profile\"></div>");
    let $profile = $("#profile");
    $profile.css(CSS_PROFILE);
    $profile.animate({"height" : "100%"}, "slow");
    $("#icone_profil").css("z-index", MENU_AND_PROFILE_Z_INDEX+1);
    profile_open = true;
    $.when(verifier_connexion()).then(function(data){
      if(data['is_connected'] === true){
        console.log("connecté"); // debug
        $profile.append("<button id='deconnexion'>Se déconnecter</button>");
        $("#deconnexion").click(function () {
          deconnexion();
          $("#nom").remove();
          accueil();
        });
      } else {
        $profile.append("<form id='connexion' method='post'>Se connecter</form>");
        let $connexion = $("#connexion");
        $connexion.append("<input type='text' id='pseudo' name='pseudo' placeholder='pseudo' required/>");
        $connexion.append("<input type='password' id='mdp' name='mdp' placeholder='motdepasse' required/>");
        $connexion.append("<input type='submit' value='Se connecter'/>");
        $profile.submit(function () {
          connexion();
          accueil(); // forcera à rouvrir le menu et à vérifier si on est connecté : évite le bourrinage du bouton connexion
          return false;
        });

        $profile.append("<form id='inscription' method='post'>Créer un compte</form>");
        let $inscription = $("#inscription");
        $inscription.append("<input type='text' id='pseudo' name='pseudo' placeholder='pseudo' required/>");
        $inscription.append("<input type='password' id='mdp' name='mdp' placeholder='motdepasse' required/>");
        $inscription.append("<input type='submit' value=\"S'inscrire\"/>");
        $inscription.submit(function(){
          inscription();
          return false;
        });
      }
    })
  }

  function connexion(){
    return $.ajax({
      url: SITE+"/php/login.php",
      type: "POST",
      data: $("#connexion").serialize(),
      dataType: "json"
    }).done(function (data) {
      console.log(data['connection_allowed']);
      if(data['connection_allowed']){
        $("#icone_profil").append("<span id='nom'>" + data['pseudo']+ "</span>")
        $("#nom").css("font-family", "serif").css("font-size", "24px");
      }
      return false; //empêche le rafraîchissement
    }).fail(function (data) {
      alert("Il y a eu un problème lors de la connexion.");
      console.log("erreur appel ajax pour la connexion");
      console.log(data);
      return false; //empêche le rafraîchissement
    });
  }

  function verifier_connexion(){
    return $.ajax({
      url: SITE + "/php/is_connected.php",
      type: "POST",
      dataType: "json"
    }).done(function(data){
      //return data['is_connected'];
      return data;
    }).fail(function(data){
      alert("Il y a eu un problème lors de la vérification de la connexion.");
      console.log("erreur appel ajax pour la vérification de la connexion");
      console.log(data);
    });
  }

  function deconnexion(){
    return $.ajax({
      url: SITE + "/php/logout.php",
      type: "POST",
      dataType: "json"
    }).done(function(data){
      console.log(data);
      return false; // empêche le rafraîchissement
    }).fail(function(data){
      alert("Il y a eu un problème lors de la déconnexion.");
      console.log("erreur appel ajax pour la déconnexion");
      console.log(data);
      return false; // empêche le rafraîchissement
    });
  }

  function inscription(){
    return $.ajax({
      url: SITE+"/php/register.php",
      type: "POST",
      data: $("#inscription").serialize(),
      dataType: "json"
    }).done(function (data) {
      let $profile = $("#profile");
      if(data['useradded'] === false){
        if(data['userexists'] === true){
          //$profile.append("<div> Problème lors de l'inscription : ce pseudo est déjà utilisé. </div>");
          alert("Ce pseudo est déjà utilisé.");
        } else {
          //$profile.append("<div>Problème lors de l'inscription : il y a un problème avec la base de données. </div>");
          alert("Il y a eu un problème avec la base de données.");
        }
      } else {
        //$profile.append("<div>Inscription réussie.</div>");
        alert("Inscription réussie.");
      }
      return false; //empêche le rafraîchissement
    }).fail(function (data) {
      console.log("erreur appel ajax pour la connexion");
      console.log(data);
      return false; //empêche le rafraîchissement
    });
  }
  
  function menu_on_click() {
    if(profile_open) { //si le profil est ouvert, on le ferme
      close_profile();
    }

    if(menu_open){ //si le menu est ouvert, on le ferme
      close_menu();
    }else{ //sinon on l'ouvre
      open_menu();
    }
  }

  function profile_on_click(){
    if(menu_open) { //si le menu est ouvert, on le ferme
      close_menu();
    }

    if(profile_open) { //si le profil est ouvert, on le ferme
      close_profile();
    }else{
      open_profile();
    }
  }

  function get_ingredients_list(){
    return $.ajax({
      url: SITE+"/ingredients.json",
      method: "POST",
      dataType: "json",
    })
        .done(function(data){
          let liste = [];
          liste.push(data["ingredients"]);
          return liste;
        }).fail(function (data) {
          console.log("problème dans l'appel ajax de la liste d'ingrédients");
    });
  }

  function add_ingredient(){
    //todo : ajout d'ingrédient dans ingredients.json
    console.log("fonction indisponible !");
  }

  function vos_cocktails_on_click(){
    //todo : vos_cocktails_on_click
    console.log("fonction indisponible !");
  }


  function rechercher_on_click(){
    // la liste des ingrédients possibles sera contenue dans un json
    let $menu = $("#menu");
    $menu.empty().animate({"width" : "99%"}, "fast");
    $menu.css(CSS_RECHERCHE_PAR_INGREDIENTS);
    $.when(get_ingredients_list()).then(function (data) {
      //quand on a récupéré la liste...
      //console.log(data["ingredients"]); //debug

      //formulaire pour les ingredients
      $menu.append("<form method='get' id='form_rechercher' />");
      let ingredients = data["ingredients"];
      for (const ingredientsKey in ingredients) { //affiche la liste des ingredients
        if(ingredients.hasOwnProperty(ingredientsKey)){
          //console.log(ingredients[ingredientsKey]); //debug
          show_ingredient($("#form_rechercher"), ingredients[ingredientsKey]);
        }
      }
      $("#form_rechercher").append("<button type='button' id='bouton_rechercher'>Rechercher des cocktails</input>");
      $("#bouton_rechercher").click(rechercher_cocktail);
    });
  }

  function rechercher_cocktail(){
    $.ajax({
      url: SITE + "/php/search_cocktail.php",
      method: "GET",
      dataType: "json",
      data: {'ingredient' : $("#form_rechercher").serializeArray()}
    }).done(function (data) {
      close_all();
      let $liste = $("#liste");
      $liste.empty();
      $liste.css(CSS_LISTE);
      console.log(data); //debug

      for (let dataKey in data){
        if(data.hasOwnProperty(dataKey)){
          if(data[dataKey] == null) continue; // en cas d'erreur, on oublie l'ingrédient actuel
          // si on a un array, c'est un cocktail :  s'il est vide, rien n'a été trouvé, on passe à l'ingrédient suivant
          if(data[dataKey].length === 0) {
            $liste.append("Aucun..." + "</br>");
            continue;
          }
          if(data[dataKey]["value"] != null){ // s'il y a "value", c'est un ingrédient
            let ingredient = data[dataKey]["value"];
            $liste.append("Cocktails trouvés avec : " + ingredient + "</br>");
          } else { // sinon, c'est un array, et donc une liste de cocktails
            for (let i in data[dataKey]) {
              let cocktail = data[dataKey][i]["nom"];
              $liste.append(cocktail + "</br>");
            }
          }
        }
      }
    })
        .fail(function (data){
          console.log("problème lors de la recherche par ingrédient (requête ajax)");
          console.log(data);
        });
  }

  function show_ingredient(div, ingredient){
    let input_to_append = "<input type ='checkbox' name='" + ingredient +"' value='" + ingredient + "'/>";
    let label_to_append = " <label for='" + ingredient + "'>" + ingredient + "</label>";
    div.append(input_to_append).append(label_to_append).append("</br></br>");
  }

  function add_cocktail(nom, difficulte, ingredients){
    $.ajax({
      url: SITE+"/php/add_cocktail.php",
      type: "POST",
      data: {'nom' : nom, 'difficulte' : difficulte, 'ingredients' : ingredients},
      dataType: "json"
    });
  }

  function close_all(){
    close_menu();
    close_profile();
  }

  function accueil(){
    close_all();
    $("#liste").empty();
    lister_cocktails();
  }


  function init_accueil(){
    $("body").css(CSS_BODY);

    // CONSTRUCTION DU HEADER
    // récupération de l'id, et nettoyage
    let $header = $("#header");
    $header.empty();
    // ajout du css et contenu + gestion des clics
    $header.css(CSS_HEADER);
    // menu
    $header.append("<span id=\"menu_icon\" class=\"material-icons\">menu</span>");
    $("#menu_icon").css(CSS_ICONS).click(menu_on_click);
    // accueil
    $header.append("<span id=\"retour_accueil\">Accueil</span>");
    $("#retour_accueil").css(CSS_BOUTON_ACCUEIL).click(accueil);
    // profil
    $header.append("<span id=\"icone_profil\" class=\"material-icons\">account_box</span>");
    $("#icone_profil").css(CSS_ICONS).click(profile_on_click);

    // CONSTRUCTION DU BODY
    // affichage de la liste des cocktails
    lister_cocktails();
  }

  $(() => {
    init_accueil();
    //let test_ingredient_add = "{\"rhum cubain\": \"6 cL\", \"jus de citron vert\": \"3 cL\"}"
    //add_cocktail("ananas", "facile", test_ingredient_add);
  })
}) ();
